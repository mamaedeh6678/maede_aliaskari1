import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatRoutingModule } from './chat-routing.module';
import { Box1Component } from './box1/box1.component';
import { Box2Component } from './box2/box2.component';
import {TableModule} from 'primeng/table';
import {FormsModule} from '@angular/forms';
import {ButtonModule} from 'primeng/button';


@NgModule({
  declarations: [Box1Component, Box2Component],
  imports: [
    CommonModule,
    ChatRoutingModule,
    TableModule,
    FormsModule,
    ButtonModule,
  ]
})
export class ChatModule { }
