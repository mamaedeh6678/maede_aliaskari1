import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Employee} from './employee/list/employee';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  getEmployees(): Observable<any> {
    return this.http.get('http://dummy.restapiexample.com/api/v1/employees');
  }

  deleteEmployee(id: number): Observable<any> {
    return this.http.delete('http://dummy.restapiexample.com/api/v1/delete/' + id);
  }
  saveEmployee(employee: Employee): Observable<any> {
    return this.http.post('http://dummy.restapiexample.com/api/v1/create', employee);
  }



}
